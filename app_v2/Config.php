<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
   protected  $fillable = ["cmpId","userId","singleApi","emailFrom","emailFromName","mailDriver","mailHost","mailPort","mailUserName","mailPassword",
     "mailEncription"];
}
