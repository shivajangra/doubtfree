<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Plan;

class MembershipController extends Controller
{
    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        // $companies = \App\Company::where('approved', '=', 0)->get();
        $plans = Plan::all(); 
        return view('admin.subscribe.index',compact('plans'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        return view('admin.subscribe.create');
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        Coupon::create($request->all());
        return redirect()->route('admin.subscribe.index');
    }


    /**
     * Show the form for editing Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        return view('admin.subscribe.edit');
    }

    /**
     * Update Role in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        return redirect()->route('admin.subscribe.index');
    }


    /**
     * Display Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        $users = \App\User::where('admin_id', $id)->get();

        $role = \App\Role::findOrFail($id);

        return view('admin.subscribe.show', compact('role', 'users'));
    }



    public function destroy($id)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();

        return redirect()->route('admin.subscribe.index');
    }


}
