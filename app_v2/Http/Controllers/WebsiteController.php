<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{
  public function index() {
    return view('/frontend.index');
  }
  public function about() {
    return view('/frontend.about');
  }
  public function ncert() {
    return view('/frontend.ncert');
  }
  public function pricing() {
    return view('/frontend.pricing');
  }
  public function contact() {
    return view('/frontend.contact');
  }
  public function privacypolicy() {
    return view('/frontend.privacypolicy');
  }
}
