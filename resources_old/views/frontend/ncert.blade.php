@extends('layouts.frontend')
@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('frontend/images/bg_1.jpg')">
  <div class="container">
  <div class="row align-items-end">
  <div class="col-lg-7">
  <h2 class="mb-0">Pricing</h2>
  <p>Welcome to our Website. We are glad to have you around.</p>
  </div>
  </div>
  </div>
</div>
<div class="custom-breadcrumns border-bottom">
  <div class="container">
  <a href="/">Home</a>
  <span class="mx-3 icon-keyboard_arrow_right"></span>
  <span class="current">Pricing</span>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="sc_heading text-center">
          <h3 style="text-transform: uppercase;" class="title">NCERT</h3>
            <p class="sub-heading" style=""> Lorem Ipsum is the dummy text... </p>
            <span class="line"></span>
          </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">For 6th standard</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Science</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Mathematics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>English</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Social Science</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">For 7th standard</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Science</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Download Pdf</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Mathematics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>English</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Social Science</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">For 8th standard</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Science</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Download Pdf</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Mathematics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>English</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Social Science</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">For 9th standard</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Physics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Download Pdf</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Chemistry</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Biology</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Mathematics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">For 10th standard</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Physics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Download Pdf</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Chemistry</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Biology</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Mathematics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">For 11th standard</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Physics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Download Pdf</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Chemistry</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Biology</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Mathematics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">For 12th standard</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Physics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Download Pdf</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Chemistry</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Biology</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <h3>Mathematics</h3>
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i> Download Pdf </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
