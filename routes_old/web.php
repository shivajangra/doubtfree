<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// website routing start here
Route::get('/', 'WebsiteController@index');
Route::get('/index1', 'WebsiteController@index1');
Route::get('/about', 'WebsiteController@about');
Route::get('/ncert', 'WebsiteController@ncert');
Route::get('/pricing', 'WebsiteController@pricing');
Route::get('/contact', 'WebsiteController@contact');
Route::get('/privacypolicy', 'WebsiteController@privacypolicy');


Route::get('/dashboard', 'AdminController@dashboard');
Route::get('/all_courses', 'AdminController@all_courses');
Route::get('/add_course', 'AdminController@add_course');
Route::get('/edit_course', 'AdminController@edit_course');
Route::get('/formfields', 'AdminController@formfields');



// admin routing start here

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
