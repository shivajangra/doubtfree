<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if(!Schema::hasTable('addresses')){
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("userid")->default(0);
            $table->string("address")->nullable();
            $table->string("state")->nullable();
            $table->string("city")->nullable();
            $table->string("pincode")->nullable();
            $table->string("address1")->nullable();
            $table->string("state1")->nullable();
            $table->string("city1")->nullable();
            $table->string("pincode1")->nullable();
            $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
