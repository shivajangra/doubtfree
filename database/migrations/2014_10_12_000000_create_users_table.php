<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if(!Schema::hasTable('users')){
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->string('prflimg')->nullable();
            $table->string('qualification')->nullable();
            $table->integer('experience')->default(0);
            $table->integer('otp')->default(0);
            $table->integer('subject')->default(0);
            $table->timestamp('dob')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('role_id')->unsigned()->nullable();
            $table->foreign('role_id', '41905_59314b1a6c90f')->references('id')->on('roles')->onDelete('cascade');
            $table->string('password');
            $table->boolean('blckflg')->default(0);
            $table->integer('frstlogin')->default(0);;
            $table->rememberToken();
            $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
