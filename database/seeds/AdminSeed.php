<?php

use Illuminate\Database\Seeder;

class AdminSeed extends Seeder
{

    public function run()
    {
        $items = [
            ['id' => 1, 'name' =>'Shiva Jangra',
              'email' => 'admin@admin.com',
              'mobile' => '8222999949',
              'role_id' => 1,
              'password' => Hash::make('password')],
        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
