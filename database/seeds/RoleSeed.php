<?php

use Illuminate\Database\Seeder;

class RoleSeed extends Seeder
{

    public function run()
    {
        $items = [

            ['id' => 1, 'title' => 'Administrator (System User)'],
            ['id' => 2, 'title' => 'Teacher'],
            ['id' => 3, 'title' => 'Student'],

        ];

        foreach ($items as $item) {
            \App\Role::create($item);
        }
    }
}
