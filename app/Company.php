<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
  protected $fillable = [
      'cmpnm', 'email', 'mobile','billtype', 'approved','emailverify','active','docupload','roomstrength','assignport','cmpchannel','calltype','pulserate','dialinpulserate','portrate','accounttype','billcycle'];
}
