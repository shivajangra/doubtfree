<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreemployeeRequest;
use App\Http\Requests\Admin\UpdateemployeeRequest;
use App\ConfRoom;


class ClientController extends Controller
{
    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        $users = DB::table('users')->Where('role_id','!=','1')
            ->get();
        return view('admin.client.index',compact('users'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        return view('admin.client.create');
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \App\Http\Requests\StoreemployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request);
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
           if($request->type == 'update'){
             $user = User::find($request->userid);
             $user->fnm = $request->fnm;
             $user->lnm = $request->lnm;
             $user->mobile = $request->mobile;
             $user->save();
             $cmp = \App\Company::find($request->cmpid);
             $cmp->cmpnm = $request->cmpnm;
             $cmp->mobile = $request->mobile;
             $cmp->billtype = $request->billtype;
             $cmp->roomstrength = $request->roomstrength;
             $cmp->pulserate = $request->pulserate;
             $cmp->dialinpulserate = $request->dialinpulserate;
             $cmp->portrate = $request->portrate;
             $cmp->save();
             $address = \App\Address::where('cmpid', $request->cmpid)->first();
             $address->address = $request->address;
             $address->city = $request->city;
             $address->state = $request->state;
             $address->pincode = $request->pincode;
             $address->pannumber = $request->pannumber;
             $address->save();
             return redirect()->back();

           }else{
             $cmp = \App\Company::findOrFail($request->cmpid);
             $request['active'] = 1;
             $cmp->update($request->all());
             $users = User::whereIn('role_id',[2,3])->where('cmpid',$cmp->id)->first();
             if($users){
               \Mail::to($users->email)->send(new \App\Mail\SubcriptionNotification($users,$cmp));
             }
             return redirect()->route('admin.client.index');
           }
    }


    public function edit($id)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        $users =  User::find($id);
        $address = \App\Address::where('userid', $id)->first();

        return view('admin.client.edit',compact('users','address'));
    }

    public function update(Request $request, $id)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        dd($id);
        return redirect()->route('admin.client.index');
    }


    public function show($id)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        $users =  User::find($id);
        $address = \App\Address::where('userid', $id)->first();

        return view('admin.client.edit',compact('users','address'));
    }

    public function destroy($id)
    {
        if (! Gate::allows('admin_access')) {
            return abort(401);
        }
        dd($id);
        $user = \App\User::findOrFail($id);
        $user->blckflg = true;
        $user->save();
        return redirect()->route('admin.client.index');
    }


}
