<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
  public function dashboard() {
    return view('/admin.dashboard');
  }
  public function all_courses() {
    return view('/admin.all_courses');
  }
  public function add_course() {
    return view('/admin.add_course');
  }
  public function edit_course() {
    return view('/admin.edit_course');
  }
  public function formfields() {
    return view('/admin.formfields');
  }
}
