<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
  protected $fillable = [
         "userid","address","state","city","pincode","address1","state1","city1","pincode1"];
}
