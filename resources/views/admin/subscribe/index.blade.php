@extends('layouts.admin')

@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">Add Course</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="/">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="#">Courses</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Add Course</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
        <form class="" action="{{route('admin.membership.store')}}" method="post">
          {{@csrf_Field()}}
          <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
              <div class="input-group">
                <input type="text" class="form-control" name="name" placeholder="Plan Name" />
              </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
              <div class="input-group">
                <input type="text" class="number form-control" name="port" placeholder="Port" />
              </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
              <div class="input-group">
                <input type="text" class="number form-control" maxlength="8" name="amount" placeholder="Amount" />
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="action_btn p-4">
                <button type="submit" name="button" class="btn btn-purple">SUBMIT</button>
                <button type="button" name="button" class="btn btn-warn empcancelbtn">CANCEL</button>
              </div>
            </div>
          </div>
        </form>
        <table class="table table-striped">
          <thead class="thead-dark">
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($plans as $key => $value)
            <tr>
              <td>{{$value->id}}</td>
              <td>{{$value->name}}</td>
              <td>
                <a href="{{ route('admin.membership.edit',[$value->id]) }}"><i class="fa fa-gear" aria-hidden="true"></i></a>
                <a href="{{ route('admin.membership.destroy',[$value->id]) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
@endsection
