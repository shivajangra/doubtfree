@extends('layouts.admin')

@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">User List</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="/">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="admin/client">Users</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">

        <table class="table table-striped">
          <thead class="thead-dark">
            <tr>
              <th>Name</th>
              <th>Mobile</th>
              <th>Email</th>
              <th>User Type</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($users as $key => $value)
            <tr>
              <td>{{$value->name}}</td>
              <td>{{$value->mobile}}</td>
              <td>{{$value->email}}</td>
              @if($value->role_id==2)
              <td>Teacher</td>
              @else
              <td>Student</td>
              @endif
              <td>
                <a href="{{ route('admin.client.show',[$value->id]) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                <a href="{{ route('admin.client.edit',[$value->id]) }}"><i class="fa fa-gear" aria-hidden="true"></i></a>
                <a href="{{ route('admin.client.destroy',[$value->id]) }}"><i class="fa fa-ban" aria-hidden="true"></i></a>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
    </div>
    </div>
@endsection
