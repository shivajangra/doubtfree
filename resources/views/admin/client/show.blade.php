@extends('layouts.admin')

@section('content')
<link rel="stylesheet" href="/css/adminpanel/common_admin.css">
<link rel="stylesheet" href="/css/adminpanel/verification.css">

<div class="pg-verification">
  <div class="left_section">
    <dl>
      <dt>
        <img src="{{ asset('images/panel/tabs/profile.png') }}" alt="Whistling" width="30" />
        {{ Auth::user()->fnm }}
        <span class="purple"><?php echo date("d-M-Y"); ?></span>
      </dt>
      <dd>
        {{\App\Role::where('id',\Auth::user()->role_id)->first()->title}} <br> ID: #BIO{{1000+ \Auth::user()->id}}

      </dd>
    </dl>
  </div>

  <div class="right_section">

    <h3 class="center purple">Profile</h3>

    <div class="row center m-4">
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <i class="fa fa-building" aria-hidden="true"></i> <br>
        <b>Company:</b> <br>
        {{ $company->cmpnm }}
      </div>
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <i class="fa fa-credit-card" aria-hidden="true"></i> <br>
        <b>GST Number:</b> <br>
        {{ $address->pannumber }}
      </div>
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <i class="fa fa-user" aria-hidden="true"></i> <br>
        <b>Name:</b> <br>
        {{ $users->fnm }} {{ $users->lnm }}
      </div>
    </div>
    <div class="row center m-4">
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <i class="fa fa-mobile" aria-hidden="true"></i> <br>
        <b>Mobile:</b> <br>
        {{ $users->mobile }}
      </div>
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <i class="fa fa-envelope" aria-hidden="true"></i> <br>
        <b>Email:</b> <br>
        {{ $users->email }}
      </div>
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        @if($company->emailverify == 1)
          <i class="fa fa-envelope" aria-hidden="true"></i> <br>
          <b>Email Verified:</b> <br>
          True
        @endif
      </div>
    </div>

    <hr>
    <h3 class="center purple">Address</h3>
    <div class="row center m-4">
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <i class="fa fa-map-marker" aria-hidden="true"></i><br>
        <b>Address:</b> <br>
        {{ $address->address }}
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <i class="fa fa-map-marker" aria-hidden="true"></i><br>
        <b>City:</b> <br>
        {{ $address->city }}
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <i class="fa fa-map-marker" aria-hidden="true"></i><br>
        <b>State:</b> <br>
        {{ $address->state }}
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <i class="fa fa-map-marker" aria-hidden="true"></i><br>
        <b>Pincode:</b> <br>
        {{ $address->pincode }}
      </div>
    </div>

    <hr>
    <div class="row center m-4">
      <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="{{ $address->panupload }}" target="_blank">
          <img src="{{ asset('images/panel/participant/download.png') }}" alt="Whistling" width="30"> Download GST Certificate
        </a>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="{{ $address->cmplogoupload }}" target="_blank">
          <img src="{{ asset('images/panel/participant/download.png') }}" alt="Whistling" width="30"> Download Company Logo
        </a>
      </div>
    </div>
    <hr>
    <h4 class="center purple">Conference Rooms</h4>
    <div class="row center m-4">
     <table class="table table-striped">
       <thead class="thead-dark">
         <tr>
           <th>Room Name</th>
           <th>Channel/Port</th>
           <th>Assigned To</th>
         </tr>
       </thead>
       <tbody>
         @foreach ($rooms as $key => $value)
         <tr>
           <td>{{$value->name}}</td>
           <td>{{$value->channels}}</td>
           <td>{{$value->assign_to}}</td>
         </tr>
         @endforeach

       </tbody>
     </table>
    </div>

  </div>
</div>


@endsection
