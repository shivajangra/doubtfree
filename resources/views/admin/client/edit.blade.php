@extends('layouts.admin')

@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title-breadcrumb">
				<div class=" pull-left">
					<div class="page-title">User List</div>
				</div>
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="/">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li><a class="parent-item" href="admin/client">Users</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
  <form class="" action="{{ route('admin.client.store') }}" method="post">
    @csrf()
    <h4 class="center purple">Profile</h4>
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <i class="fa fa-user" aria-hidden="true"></i> <br>
        <b>Name:</b> <br>
        <input type="text" class="form-control" name="fnm" value="{{isset($users->fnm)?$users->fnm:''}}">
      </div>
    <!-- </div>
    <div class="row center m-4"> -->
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <i class="fa fa-mobile" aria-hidden="true"></i> <br>
        <b>Mobile:</b> <br>
        <input type="text" class="number form-control" name="mobile" value="{{isset($users->mobile)?$users->mobile:''}}">
      </div>
      <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <i class="fa fa-envelope" aria-hidden="true"></i> <br>
        <b>Email:</b> <br>
        <input type="text" readonly class="form-control" name="email" value="{{isset($users->email)?$users->email:''}}">
      </div>
    </div>

    <!-- <hr> -->
    <h4 class="center purple">Address</h4>
    <div class="row center m-4">
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <i class="fa fa-map-marker" aria-hidden="true"></i><br>
        <b>Address:</b> <br>
        <input type="text" class="form-control" name="address" value="{{isset($address->address)?$address->address:''}}">
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <i class="fa fa-map-marker" aria-hidden="true"></i><br>
        <b>City:</b> <br>
        <input type="text" class="form-control" name="city" value="{{isset($address->city)?$address->city:''}}">
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <i class="fa fa-map-marker" aria-hidden="true"></i><br>
        <b>State:</b> <br>
        <input type="text" class="form-control" name="state" value="{{isset($address->state)?$address->state:''}}">
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <i class="fa fa-map-marker" aria-hidden="true"></i><br>
        <b>Pincode:</b> <br>
        <input type="text" class="form-control" name="pincode" value="{{isset($address->pincode)?$address->pincode:''}}">
      </div>
    </div>


      </div>
    </div>
  </div>
</div>


@endsection
