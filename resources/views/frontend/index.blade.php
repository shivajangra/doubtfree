@extends('layouts.frontend')
@section('content')


<div class="hero-slide owl-carousel site-blocks-cover">
  <div class="intro-section" style="background-image: url('frontend/images/hero_1.jpg');">
    <div class="container">
    <div class="row align-items-center">
    <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
    <h1>Doubtfree- Key of Success</h1>
    </div>
    </div>
    </div>
  </div>
  <div class="intro-section" style="background-image: url('frontend/images/hero_1.jpg');">
    <div class="container">
    <div class="row align-items-center">
    <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
    <h1>You Can Learn Anything</h1>
    </div>
    </div>
    </div>
  </div>
</div>

<!-- <div></div> -->



<div class="news-updates">
  <div class="container">
    <div class="row">
      <div class="col-lg-9">
        <div class="section-heading">
          <h2 class="text-black">Notifications &amp; Updates</h2>
          <a href="#">Read All Updates</a>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <div class="post-entry-big">
              <a href="news-single.html" class="img-link"><img src="frontend/images/blog_large_1.jpg" alt="Image" class="img-fluid"></a>
              <div class="post-content">
                <div class="post-meta">
                  <a href="#">Sep 16, 2020</a>
                  <span class="mx-1">/</span>
                  <a href="#">Courses</a>, <a href="#">Updates</a>
                </div>
                <h3 class="post-heading"><a href="news-single.html">Doubt sessions start from 15<sup>th</sup> September</a></h3>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="post-entry-big horizontal d-flex mb-4">
              <a href="news-single.html" class="img-link mr-4"><img src="frontend/images/blog_1.jpg" alt="Image" class="img-fluid"></a>
              <div class="post-content">
                <div class="post-meta">
                  <a href="#">Sep 16, 2020</a>
                  <span class="mx-1">/</span>
                  <a href="#">Courses</a>, <a href="#">Updates</a>
                </div>
                <h3 class="post-heading"><a href="news-single.html">Free career couselling for all students</a></h3>
              </div>
            </div>
            <div class="post-entry-big horizontal d-flex mb-4">
              <a href="news-single.html" class="img-link mr-4"><img src="frontend/images/blog_2.jpg" alt="Image" class="img-fluid"></a>
              <div class="post-content">
                <div class="post-meta">
                  <a href="#">Sep 16, 2020</a>
                  <span class="mx-1">/</span>
                  <a href="#">Courses</a>, <a href="#">Updates</a>
                </div>
                <h3 class="post-heading"><a href="news-single.html">Full Josaa counselling at rupees 1000/- only</a></h3>
              </div>
            </div>
            <div class="post-entry-big horizontal d-flex mb-4">
              <a href="news-single.html" class="img-link mr-4"><img src="frontend/images/blog_1.jpg" alt="Image" class="img-fluid"></a>
              <div class="post-content">
                <div class="post-meta">
                  <a href="#">Sep 16, 2020</a>
                  <span class="mx-1">/</span>
                  <a href="#">Admission</a>, <a href="#">Updates</a>
                </div>
                <h3 class="post-heading"><a href="news-single.html">Free career couselling for subscriber</a></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="section-heading">
          <h2 class="text-black">Campus Videos</h2>
          <a href="#">View All Videos</a>
        </div>
        <a href="https://vimeo.com/45830194" class="video-1 mb-4" data-fancybox="" data-ratio="2">
          <span class="play">
            <span class="icon-play"></span>
          </span>
          <img src="frontend/images/course_5.jpg" alt="Image" class="img-fluid">
        </a>
        <a href="https://vimeo.com/45830194" class="video-1 mb-4" data-fancybox="" data-ratio="2">
          <span class="play">
            <span class="icon-play"></span>
          </span>
          <img src="images/course_5.jpg" alt="Image" class="img-fluid">
        </a>
      </div>
    </div>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <div class="row mb-5 justify-content-center text-center">
      <div class="col-lg-4 mb-5">
        <h2 class="section-title-underline mb-5">
          <span>Kota ke Notes</span>
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-mortarboard text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Physics</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit</p>
            <p><a href="kotanotes" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-library text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Chemistry</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit</p>
            <p><a href="kotanotes" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-school-material text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Mathematics</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit</p>
            <p><a href="kotanotes" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <div class="row justify-content-center text-center">
      <div class="col-lg-6">
        <h2 class="section-title-underline mb-3">
          <span>NCERT</span>
        </h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, id?</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="owl-slide-3 owl-carousel">
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="course-single.html"><img src="frontend/images/course_1.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">₹ 1100 p.a.</div>
            <div class="category"><h3>For 6<sup>th</sup> Standard</h3></div>
          </figure>
          <div class="course-1-content pb-4">
            <h2>Choose Subjects @ affordable prices</h2>

            <!-- <div class="rating text-center mb-3">
              <span class="icon-star2 text-warning"></span>
              <span class="icon-star2 text-warning"></span>
              <span class="icon-star2 text-warning"></span>
              <span class="icon-star2 text-warning"></span>
              <span class="icon-star2 text-warning"></span>
            </div> -->
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Mathematics</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Science</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> English</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Hindi</p>
            <p><a href="course-single.html" class="btn btn-primary rounded-0 px-4">Enroll</a></p>
          </div>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
          <a href="course-single.html"><img src="frontend/images/course_2.jpg" alt="Image" class="img-fluid"></a>
          <div class="price">₹ 1100 p.a.</div>
          <div class="category"><h3>For 7<sup>th</sup> Standard</h3></div>
          </figure>
          <div class="course-1-content pb-4">
            <h2>Choose Subjects @ affordable prices</h2>

            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Mathematics</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Science</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> English</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Hindi</p>
            <p><a href="course-single.html" class="btn btn-primary rounded-0 px-4">Enroll</a></p>
          </div>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="course-single.html"><img src="frontend/images/course_3.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">₹ 1100 p.a.</div>
            <div class="category"><h3>For 8<sup>th</sup> Standard</h3></div>
          </figure>
          <div class="course-1-content pb-4">
            <h2>Choose Subjects @ affordable prices</h2>

            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Mathematics</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Physics</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Chemistry</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Biology</p>
            <p><a href="courses-single.html" class="btn btn-primary rounded-0 px-4">Enroll</a></p>
          </div>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="course-single.html"><img src="frontend/images/course_4.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">₹ 1100 p.a.</div>
            <div class="category"><h3>For 9<sup>th</sup> Standard</h3></div>
          </figure>
          <div class="course-1-content pb-4">
            <h2>Choose Subjects @ affordable prices</h2>

            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Mathematics</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Physics</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Chemistry</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Biology</p>
            <p><a href="course-single.html" class="btn btn-primary rounded-0 px-4">Enroll</a></p>
          </div>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="course-single.html"><img src="frontend/images/course_5.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">₹ 1100 p.a.</div>
            <div class="category"><h3>For 10<sup>th</sup> Standard</h3></div>
          </figure>
          <div class="course-1-content pb-4">
            <h2>Choose Subjects @ affordable prices</h2>

            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Mathematics</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Physics</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Chemistry</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Biology</p>
            <p><a href="course-single.html" class="btn btn-primary rounded-0 px-4">Enroll</a></p>
          </div>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="course-single.html"><img src="frontend/images/course_6.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">₹ 1100 p.a.</div>
            <div class="category"><h3>For 11<sup>th</sup> Standard</h3></div>
          </figure>
          <div class="course-1-content pb-4">
            <h2>Choose Subjects @ affordable prices</h2>

            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Medical</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Non-medical</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Commerce</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Arts</p>
            <p><a href="course-single.html" class="btn btn-primary rounded-0 px-4">Enroll</a></p>
          </div>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="course-single.html"><img src="frontend/images/course_6.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">₹ 1100 p.a.</div>
            <div class="category"><h3>For 12<sup>th</sup> Standard</h3></div>
          </figure>
          <div class="course-1-content pb-4">
            <h2>Choose Subjects @ affordable prices</h2>

            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Medical</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Non-medical</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Commerce</p>
            <p class="desc mb-4"><input type="checkbox" name="" value="" /> Arts</p>
            <p><a href="course-single.html" class="btn btn-primary rounded-0 px-4">Enroll</a></p>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="site-section">
  <div class="container">
    <div class="row justify-content-center text-center">
      <div class="col-lg-6">
        <h2 class="section-title-underline mb-3">
          <span>TOP COLLEGE LIST</span>
        </h2>
        <p>Education all over the India.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="owl-slide-3 owl-carousel">
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="#"><img src="frontend/images/iit_colg.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">Download PDF</div>
            <div class="category"><h3>IIT</h3></div>
          </figure>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
          <a href="#"><img src="frontend/images/iiit_colg.jpg" alt="Image" class="img-fluid"></a>
          <div class="price">Download PDF</div>
          <div class="category"><h3>IIIT</h3></div>
          </figure>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="#"><img src="frontend/images/nit_colg.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">Download PDF</div>
            <div class="category"><h3>NIT</h3></div>
          </figure>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="#"><img src="frontend/images/gfti_colg.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">Download PDF</div>
            <div class="category"><h3>GFTI</h3></div>
          </figure>
        </div>
        <div class="course-1-item">
          <figure class="thumnail">
            <a href="#"><img src="frontend/images/course_5.jpg" alt="Image" class="img-fluid"></a>
            <div class="price">Download PDF</div>
            <div class="category"><h3>Private College</h3></div>
          </figure>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <div class="row mb-5 justify-content-center text-center">
      <div class="col-lg-6">
        <h2 class="section-title-underline">
          <span>For JEE Preparation</span>
        </h2>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. </p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="feature-1 border">
          <!-- <div class="icon-wrapper bg-primary">
            <span class="flaticon-mortarboard text-white"></span>
          </div> -->
          <div class="feature-1-content">
            <h2>Physics</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit</p>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Download Pdf</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="feature-1 border">
          <!-- <div class="icon-wrapper bg-primary">
            <span class="flaticon-library text-white"></span>
          </div> -->
          <div class="feature-1-content">
            <h2>Chemistry</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit</p>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Download Pdf</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
        <div class="feature-1 border">
          <!-- <div class="icon-wrapper bg-primary">
            <span class="flaticon-school-material text-white"></span>
          </div> -->
          <div class="feature-1-content">
            <h2>Mathematics</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi hendrerit elit</p>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Download Pdf</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <div class="section-bg style-1" style="background-image: url('frontend/images/about_1.html');">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <h2 class="section-title-underline style-2">
          <span>About Doubtfree</span>
        </h2>
      </div>
      <div class="col-lg-8">
        <p class="lead">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem nesciunt quaerat ad reiciendis perferendis voluptate fugiat sunt fuga error totam.</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus assumenda omnis tempora ullam alias amet eveniet voluptas, incidunt quasi aut officiis porro ad, expedita saepe necessitatibus rem debitis architecto dolore? Nam omnis sapiente placeat blanditiis voluptas dignissimos, itaque fugit a laudantium adipisci dolorem enim ipsum cum molestias? Quod quae molestias modi fugiat quisquam. Eligendi recusandae officiis debitis quas beatae aliquam?</p>
        <p><a href="#">Read more</a></p>
      </div>
    </div>
  </div>
</div> -->

<div class="section-bg style-1" style="background-image: url('frontend/images/hero_1.jpg');">
  <div class="container">
    <div class="row mb-5 justify-content-center text-center">
      <div class="col-lg-6">
        <h2 class="section-title-underline">
          <span>PREVIOUS YEAR QUESTION PAPER</span>
        </h2>
        <p>Previous Year Question Paper to feed your brain.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
        <!-- <span class="icon flaticon-mortarboard"></span> -->
        <h3>JEE Mains</h3>
        <p>Get IIT JEE mains previous year question paper with solutions for Maths yearwise.</p>
        <p><a href="#" class="btn btn-primary px-4 rounded-0">Download Pdf</a></p>
      </div>
      <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
        <!-- <span class="icon flaticon-school-material"></span> -->
        <h3>JEE Advanced</h3>
        <p>Get JEE Advanced previous year question paper with solutions for Maths yearwise.</p>
        <p><a href="#" class="btn btn-primary px-4 rounded-0">Download Pdf</a></p>
      </div>
      <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
        <!-- <span class="icon flaticon-library"></span> -->
        <h3>WBJEE</h3>
        <p>Get WBJEE previous year question paper with solutions for Maths yearwise.</p>
        <p><a href="#" class="btn btn-primary px-4 rounded-0">Download Pdf</a></p>
      </div>
    </div>
  </div>
</div>


<div class="site-section">
  <div class="container">
    <div class="row mb-5">
      <div class="col-lg-4">
        <h2 class="section-title-underline">
          <span>Testimonials</span>
        </h2>
        <p>How people said about Doubtfree.</p>
      </div>
    </div>
    <div class="owl-slide owl-carousel">
      <div class="ftco-testimonial-1">
        <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
          <img src="frontend/images/person_1.jpg" alt="Image" class="img-fluid mr-3">
          <div>
            <h3>Suresh Sharma</h3>
            <span>JEE Advanced</span>
          </div>
        </div>
        <div>
        <p>&ldquo; I feel really lucky to be a part of such a great institute. Here teachers are very helpful and hard working. I heartily thank the Doubtfree team to give me the way to achieving my goal." "An Amazing institute for JEE aspirants for JEE Advanced and JEE Main. &rdquo;</p>
        </div>
      </div>
      <div class="ftco-testimonial-1">
        <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
          <img src="frontend/images/person_3.jpg" alt="Image" class="img-fluid mr-3">
          <div>
            <h3>Shivam</h3>
            <span>JEE Mains</span>
          </div>
        </div>
        <div>
          <p>&ldquo; I enjoyed being a student of Doubtfree. Teachers here are quite hardworking and helpful. Here, I found the best faculty I ever had in my life. I feel so lucky to have such amazing teachers. I am thankful to them for guiding me, inspiring me to achieve my goals. I wish I could get your guidance throughout my life. &rdquo;</p>
        </div>
      </div>
      <div class="ftco-testimonial-1">
        <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
          <img src="frontend/images/person_1.jpg" alt="Image" class="img-fluid mr-3">
          <div>
            <h3>Abhishek</h3>
            <span>12th Standard</span>
          </div>
        </div>
        <div>
          <p>&ldquo; I strongly recommend Doubtfree for preparation of jee which have excellent teaching with highly experienced faculties ... their painstaking efforts and hard works provide us platform to get into IITs &rdquo;</p>
        </div>
      </div>
      <div class="ftco-testimonial-1">
        <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
          <img src="frontend/images/person_3.jpg" alt="Image" class="img-fluid mr-3">
          <div>
            <h3>Mr. Mathur</h3>
            <span>Parent</span>
          </div>
        </div>
        <div>
          <p>&ldquo; Really good results every year given by Doubtfree jee coaching. there faculty team really best. &rdquo;</p>
        </div>
      </div>
      <div class="ftco-testimonial-1">
        <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
          <img src="frontend/images/person_2.jpg" alt="Image" class="img-fluid mr-3">
          <div>
            <h3>Anju</h3>
            <span>10th Standard</span>
          </div>
        </div>
        <div>
        <p>&ldquo; It was a good experience to be a part of Doubtfree. They all are very helpful and friendly and helps us in clearing every doubt. Teachers always supportive and cleared all the concepts to us. &rdquo;</p>
        </div>
      </div>
    </div>
  </div>
</div>





@endsection
