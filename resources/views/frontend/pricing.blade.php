@extends('layouts.frontend')
@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('frontend/images/bg_1.jpg')">
  <div class="container">
  <div class="row align-items-end">
  <div class="col-lg-7">
  <h2 class="mb-0">Pricing</h2>
  <p>Welcome to our Website. We are glad to have you around.</p>
  </div>
  </div>
  </div>
</div>
<div class="custom-breadcrumns border-bottom">
  <div class="container">
  <a href="/">Home</a>
  <span class="mx-3 icon-keyboard_arrow_right"></span>
  <span class="current">Pricing</span>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="sc_heading text-center">
          <h3 style="text-transform: uppercase;" class="title">Pricing</h3>
            <p class="sub-heading" style="">Choose Packages @ affordable prices for the courses</p><span class="line"></span>
          </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">For 6th & 7th standard</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>3 Month :</b> ₹ 500
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>6 Month :</b> ₹ 800
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>1 Year :</b> ₹ 1100
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>2 Year :</b> ₹ 2500
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title"> For 8th to 10th standard </h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>3 Month :</b> ₹ 800
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>6 Month :</b> ₹ 1400
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>1 Year :</b> ₹ 2600
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>2 Year :</b> ₹ 4799
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title"> For 11th, 12th standard </h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>3 Month :</b> ₹ 600
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>6 Month :</b> ₹ 999
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>1 Year :</b> ₹ 1799
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>2 Year :</b> ₹ 3200
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-6">
      <div class="col-lg-12 mb-2">
          <h3 class="widget-title">Prices Of JEE & WBJEE</h3>
      </div>

      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>3 Month :</b> ₹ 600
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>

          <!-- <p> <b>6 Month :</b> ₹ 999 </p>
          <p> <b>1 Year :</b> ₹ 1799 </p>
          <p> <b>2 Year :</b> ₹ 3200 </p> -->
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>6 Month :</b> ₹ 999
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>1 Year :</b> ₹ 1799
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="schlprice box_shadow">
          <div class="text-center">
            <b>2 Year :</b> ₹ 3200
            <div class="tp_chameleon_customize mt-2">
              <div class="tp-buy-theme">
                <a class="link-buy" href="#" title="Buy Now"><i class="fa fa-shopping-cart"></i>Buy Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
