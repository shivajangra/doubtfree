<!DOCTYPE html>
<html lang="en">
<head>
<title>Doubtfree : Ab Padhna Hua Asaaan</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="keywords" content="Doubtfree : Ab Padhna Hua Asaaan, Doubtfree best education site, training center, education center, courses hub and academy.">
<meta name="description" content="Doubtfree : Ab Padhna Hua Asaaan, Doubtfree best education site, training center, education center, courses hub and academy.">
<meta name="author" content="Doubtfree : Ab Padhna Hua Asaaan">

<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
<link rel="stylesheet" href="frontend/fonts/icomoon/style.css">
<link rel="stylesheet" href="frontend/css/bootstrap.min.css">
<link rel="stylesheet" href="frontend/css/jquery-ui.css">
<link rel="stylesheet" href="frontend/css/owl.carousel.min.css">
<link rel="stylesheet" href="frontend/css/owl.theme.default.min.css">
<link rel="stylesheet" href="frontend/css/owl.theme.default.min.css">
<link rel="stylesheet" href="frontend/css/jquery.fancybox.min.css">
<link rel="stylesheet" href="frontend/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="frontend/fonts/flaticon/font/flaticon.css">
<link rel="stylesheet" href="frontend/css/aos.css">
<link href="frontend/css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="frontend/css/style.css">
<link rel="stylesheet" href="frontend/css/custom.css">
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

  <div class="site-wrap">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    <div class="py-2 bg-light">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-9 d-none d-lg-block">
            <a href="#" class="small mr-3"><span class="icon-question-circle-o mr-2"></span> Have a questions?</a>
            <a href="tel:6204061389" class="small mr-3"><span class="icon-phone2 mr-2"></span> 6204061389 </a>
            <a href="mailto:doubtfree23@gmail.com" class="small mr-3"><span class="icon-envelope-o mr-2"></span> <span class="__cf_email__">doubtfree23@gmail.com</span></a>
          </div>
          <div class="col-lg-3 text-right">
            <a href="/login" class="small mr-3"><span class="icon-unlock-alt"></span> Log In</a>
            <a href="/register" class="small btn btn-primary px-4 py-2 rounded-0"><span class="icon-users"></span> Register</a>
          </div>
        </div>
      </div>
    </div>
    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">
      <div class="container">
        <div class="d-flex align-items-center">
          <div class="site-logo">
            <a href="/" class="d-block">
              <img src="frontend/images/logo_doubtfree.jpg" alt="Image" class="img-fluid" width="80">
            </a>
          </div>
          <div class="mr-auto">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li class="active">
                  <a href="/" class="nav-link text-left">Home</a>
                </li>
                <li class="has-children">
                  <a href="about.html" class="nav-link text-left">About</a>
                  <ul class="dropdown">
                    <li><a href="teachers.html">Our Teachers</a></li>
                    <li><a href="about.html">Our Students</a></li>
                  </ul>
                </li>
                <li class="has-children">
                  <a href="ncert" class="nav-link text-left">NCERT</a>
                  <ul class="dropdown">
                    <li><a href="ncert">For 6th Standard</a></li>
                    <li><a href="ncert">For 7th Standard</a></li>
                    <li><a href="ncert">For 8th Standard</a></li>
                    <li><a href="ncert">For 9th Standard</a></li>
                    <li><a href="ncert">For 10th Standard</a></li>
                    <li><a href="ncert">For 11th Standard</a></li>
                    <li><a href="ncert">For 12th Standard</a></li>
                  </ul>
                </li>
                <li class="has-children">
                  <a href="courses" class="nav-link text-left">Courses</a>
                  <ul class="dropdown">
                    <li><a href="courses">JEE Mains</a></li>
                    <li><a href="courses">JEE Advanced</a></li>
                    <li><a href="courses">WBJEE</a></li>
                    <li><a href="courses">NEET</a></li>
                  </ul>
                </li>
                <li>
                  <a href="pricing" class="nav-link text-left">Pricing</a>
                </li>
                <li>
                  <a href="contact" class="nav-link text-left">Contact</a>
                </li>
              </ul>
              <!-- </ul> -->
            </nav>
          </div>
          <div class="ml-auto">
            <div class="social-wrap">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <a href="#"><span class="icon-linkedin"></span></a>
              <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>
            </div>
          </div>
        </div>
      </div>
    </header>

    <main>
        @yield('content')
    </main>

    <div class="site-section ftco-subscribe-1" style="background-image: url('frontend/images/bg_1.jpg')">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-7">
            <h2>Subscribe to us!</h2>
            <p>Subscribe now and receive weekly newsletter with educational materials, new courses and much more!</p>
          </div>
          <div class="col-lg-5">
            <form action="#" class="d-flex">
              <input type="text" class="rounded form-control mr-2 py-3" placeholder="Enter your email">
              <button class="btn btn-primary rounded py-3 px-4" type="submit">Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
            <p class="mb-4"><a href="/"><img src="frontend/images/logo_doubtfree.jpg" alt="Image" class="img-fluid" width="80"></a></p>
            <p><a href="about">Growth your career with Doubtfree. Growth your career with Doubtfree.</a></p>
            <!-- <p><a href="about">Learn More</a></p> -->
          </div>
          <div class="col-lg-3">
            <h3 class="footer-heading"><span> Links </span></h3>
            <ul class="list-unstyled">
              <li><a href="about">About</a></li>
              <li><a href="pricing">Pricing</a></li>
              <li><a href="contact">Contact</a></li>
              <li><a href="news">News</a></li>
              <li><a href="privacypolicy">Privacy Policy</a></li>
            </ul>
          </div>
          <div class="col-lg-3">
            <h3 class="footer-heading"><span> NCERT </span></h3>
            <ul class="list-unstyled">
              <li><a href="ncert">Mathematics</a></li>
              <li><a href="ncert">Physics </a></li>
              <li><a href="ncert">Chemistry</a></li>
              <li><a href="ncert">Biology</a></li>
              <li><a href="ncert">English</a></li>
            </ul>
          </div>
          <div class="col-lg-3">
            <h3 class="footer-heading"><span>Our Courses</span></h3>
            <ul class="list-unstyled">
              <li><a href="#">JEE Mains</a></li>
              <li><a href="#">JEE Advanced</a></li>
              <li><a href="#">WBJEE</a></li>
              <li><a href="#">NEET</a></li>
              <li><a href="#">Kota ke Notes</a></li>
              <li><a href="#">Previous Question Papers</a></li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="copyright">
              <p> Copyright &copy;<script data-cfasync="false" src="frontend/js/email-decode.min.js"></script><script type="text/javascript">document.write(new Date().getFullYear());</script>. | All rights reserved. | Designed by <a href="http://doubtfree.com/" target="_blank">Doubtfree</a>. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- <div id="loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" /><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#51be78" />
    </svg>
  </div> -->
  <script src="frontend/js/jquery-3.3.1.min.js" type="text/javascript"></script>
  <script src="frontend/js/jquery-migrate-3.0.1.min.js" type="text/javascript"></script>
  <script src="frontend/js/jquery-ui.js" type="text/javascript"></script>
  <script src="frontend/js/popper.min.js" type="text/javascript"></script>
  <script src="frontend/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="frontend/js/owl.carousel.min.js" type="text/javascript"></script>
  <script src="frontend/js/jquery.stellar.min.js" type="text/javascript"></script>
  <script src="frontend/js/jquery.countdown.min.js" type="text/javascript"></script>
  <script src="frontend/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="frontend/js/jquery.easing.1.3.js" type="text/javascript"></script>
  <script src="frontend/js/aos.js" type="text/javascript"></script>
  <script src="frontend/js/jquery.fancybox.min.js" type="text/javascript"></script>
  <script src="frontend/js/jquery.sticky.js" type="text/javascript"></script>
  <script src="frontend/js/jquery.mb.YTPlayer.min.js" type="text/javascript"></script>
  <script src="frontend/js/main.js" type="text/javascript"></script>

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="text/javascript"></script>
  <script type="text/javascript">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
  </script>
  <script src="frontend/js/rocket-loader.min.js" data-cf-settings="3c44e6433a3bb757193703ff-|49" defer=""></script>
</body>
</html>
